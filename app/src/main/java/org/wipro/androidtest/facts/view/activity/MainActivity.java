package org.wipro.androidtest.facts.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.wipro.androidtest.R;
import org.wipro.androidtest.api_utils.APIServiceFactory;
import org.wipro.androidtest.common.CustomView;
import org.wipro.androidtest.facts.interfaces.CommonService;
import org.wipro.androidtest.facts.model.FactsModel;
import org.wipro.androidtest.facts.model.Rows;
import org.wipro.androidtest.facts.view.adapter.FactsAdapter;
import org.wipro.androidtest.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.wipro.androidtest.utils.Helper.isNetworkAvailable;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.dataRecycler)
    RecyclerView dataRecycler;
    @BindView(R.id.txt_title)
    TextView txt_title;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    public Context mContext = this;
    private CommonService commonService;
    private ProgressDialog progressDialog;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setBackgroundDrawable(null);
        actionBarSetting();
        ButterKnife.bind(this);
        commonService = APIServiceFactory.createService(CommonService.class);
        init();
    }

    //set header details
    private void actionBarSetting() {
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View lView = mInflater.inflate(R.layout.toolbar, null);
        mActionBar.setCustomView(lView);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    private void init() {
        getAPICall();
        //listner for swipr down refreshlayout
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAPICall();
            }
        });

    }


    //call api
    public void getAPICall() {

        //check imternet service
        if (!isNetworkAvailable(mContext)) {
            Toast.makeText(mContext,Constant.Network_Error,Toast.LENGTH_SHORT).show();
            refreshLayout.setRefreshing(false);
            return;
        }


        progressDialog = CustomView.ShowProgressDialog(mContext, mContext.getResources().getString(R.string.loading));
        progressDialog.show();



        Call<FactsModel> call = commonService.getFacts();
        String url = call.request().url().toString();
        Log.d("url : ", url);
        call.enqueue(new Callback<FactsModel>() {
            @Override
            public void onResponse(Call<FactsModel> call, @NonNull Response<FactsModel> response) {

                progressDialog.dismiss();
                if (response.isSuccessful() && response.body() != null && response.body().getRows().length >0 ) {
                    // create object of model
                    FactsModel factsModel = response.body();
                    Rows[] modeldata = factsModel.getRows();
                    txt_title.setText(response.body().getTitle());
                    LinearLayoutManager lLinearLayoutManager = new LinearLayoutManager(MainActivity.this);
                    lLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    dataRecycler.setHasFixedSize(true);
                    dataRecycler.setLayoutManager(lLinearLayoutManager);

                    //create and attaxh adapter
                    FactsAdapter factsAdapter = new FactsAdapter(mContext, this, modeldata);
                    dataRecycler.setAdapter(factsAdapter);


                } else {
                        //gettimg null response.
                        Toast.makeText(mContext,getResources().getString(R.string.no_response),Toast.LENGTH_SHORT).show();

                }

                // Stopping swipe refresh
                refreshLayout.setRefreshing(false);
            }

            //api call failure
            @Override
            public void onFailure(Call<FactsModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(mContext,getResources().getString(R.string.api_fail),Toast.LENGTH_SHORT).show();
                refreshLayout.setRefreshing(false);
            }
        });

    }
}
