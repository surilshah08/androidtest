package org.wipro.androidtest.facts.interfaces;

import android.support.annotation.NonNull;

import org.wipro.androidtest.api_utils.APIConstant;
import org.wipro.androidtest.facts.model.FactsModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CommonService {

    @NonNull
    @GET(APIConstant.GET_FACTS_API)
    Call<FactsModel> getFacts(

    );
}
