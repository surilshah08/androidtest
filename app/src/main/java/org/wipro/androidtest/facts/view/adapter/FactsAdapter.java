package org.wipro.androidtest.facts.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.wipro.androidtest.R;
import org.wipro.androidtest.facts.model.FactsModel;
import org.wipro.androidtest.facts.model.Rows;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;

public class FactsAdapter extends RecyclerView.Adapter<FactsAdapter.ViewHolder>{
    private Context mContext;
    private Callback<FactsModel> callback;
    private Rows[] modeldata;

    public FactsAdapter(Context mContext, Callback<FactsModel> callback, Rows[] modeldata) {
        this.mContext =mContext;
        this.callback =callback;
        this.modeldata=modeldata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item_facts, parent, false);
        return new FactsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


            holder.cardView.setVisibility(View.VISIBLE);
            holder.itemTitleTextview.setText(modeldata[position].getTitle());
            holder.itemDescriptionTextview.setText(modeldata[position].getDescription());
            if (modeldata[position].getImageHref() != null && modeldata[position].getImageHref().length() > 0) {

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.mipmap.ic_launcher);
                requestOptions.error(R.mipmap.ic_launcher);

                //set image from url
                Glide
                        .with(mContext)
                        .setDefaultRequestOptions(requestOptions)
                        .load(modeldata[position].getImageHref())
                        .into(holder.itemImageview);

            }


    }

    @Override
    public int getItemCount() {
        return modeldata.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardview)
        CardView cardView;
        @BindView(R.id.itemTitleTextview)
        TextView itemTitleTextview;
        @BindView(R.id.itemDescriptionTextview)
        TextView itemDescriptionTextview;
        @BindView(R.id.itemImageview)
        ImageView itemImageview;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
