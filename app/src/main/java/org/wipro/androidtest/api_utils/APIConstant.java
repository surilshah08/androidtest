package org.wipro.androidtest.api_utils;

import android.support.annotation.NonNull;

public class APIConstant {

    @NonNull
    public static final String SERVER_URL = "https://dl.dropboxusercontent.com/";

    //get_facts
    public static final String GET_FACTS_API = "s/2iodh4vg0eortkl/facts.json";


    //Response Status Code
    public static final int SUCCESS_CODE = 200;
}
