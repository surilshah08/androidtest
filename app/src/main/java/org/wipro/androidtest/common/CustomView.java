package org.wipro.androidtest.common;

import android.app.ProgressDialog;
import android.content.Context;

public class CustomView
{
  //prohress dialog
  public static ProgressDialog ShowProgressDialog(Context paramContext, String paramString)
  {
    ProgressDialog localProgressDialog = new ProgressDialog(paramContext);
    localProgressDialog.setCancelable(true);
    localProgressDialog.setMessage(paramString);
    localProgressDialog.isIndeterminate();
    localProgressDialog.setCanceledOnTouchOutside(false);
    return localProgressDialog;
  }
  

}


